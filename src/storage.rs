use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use lazy_static::lazy_static;
use rand::Rng;
use rocket::http::uri::fmt::{Formatter, FromUriParam, Path, UriDisplay};
use rocket::request::FromParam;
use serde::{Deserialize, Serialize};
use sled::Db;
use typed_sled::Tree;

pub type ID = u128;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Post {
    pub parent: ID,
    pub id: ID,
    pub posts: u64,
    
    pub depth: usize,

    #[serde(with = "ts_seconds")]
    pub timestamp: DateTime<Utc>,

    pub text: String,
    pub author: Option<String>,
}

lazy_static! {
    pub static ref DATABASE: Database = Database::new();
}

pub struct Database {
    pub db: Db,
    pub posts: Tree<ID, Post>,
    pub subposts: Tree<(ID, ID), ()>,
}

impl Database {
    pub fn new() -> Self {
        let db = sled::open("data").unwrap();
        Self {
            posts: typed_sled::Tree::open(&db, "posts"),
            subposts: typed_sled::Tree::open(&db, "subposts"),
            db,
        }
    }
}

impl Post {
    pub fn root() -> Post {
        Post::load(0).unwrap()
    }

    pub fn load(id: ID) -> Option<Post> {
        DATABASE.posts.get(&id).unwrap()
    }

    pub fn subposts(&self) -> impl DoubleEndedIterator<Item = Post> {
        DATABASE
            .subposts
            .range((self.id, ID::MIN)..(self.id, ID::MAX))
            .map(|e| e.unwrap().0 .1)
            .map(Post::load)
            .filter_map(|e| e)
    }

    pub fn subpost_template(&self) -> Post {
        Post {
            parent: self.id,
            id: rand::thread_rng().gen(),
            posts: 0,
            timestamp: Utc::now(),
            text: String::from("<unnamed>"),
            author: None,
            depth: self.depth + 1,
        }
    }
    pub fn insert(&self) {
        // insert into post listing
        DATABASE.posts.insert(&self.id, &self).unwrap();
        DATABASE
            .subposts
            .insert(&(self.parent, self.id), &())
            .unwrap();

        // increment post count in the parent
        DATABASE
            .posts
            .update_and_fetch(&self.parent, |t| {
                let mut t = t.unwrap().clone();
                t.posts += 1;
                Some(t)
            })
            .unwrap()
            .unwrap();
    }
    pub fn idstring(&self) -> String {
        format!("{}", self.id)
    }
    pub fn parentstring(&self) -> String {
        format!("{}", self.parent)
    }
}

impl FromParam<'_> for Post {
    type Error = String;

    fn from_param(id: &str) -> Result<Self, Self::Error> {
        Post::load(id.parse().map_err(|e| format!("{}", e))?).ok_or(String::from("post not found"))
    }
}

impl UriDisplay<Path> for Post {
    fn fmt(&self, f: &mut Formatter<Path>) -> std::fmt::Result {
        f.write_value(self.id)
    }
}

impl<'a> FromUriParam<Path, &'a str> for Post {
    type Target = Post;

    fn from_uri_param(id: &'a str) -> Post {
        Post::load(id.parse().map_err(|e| format!("{}", e)).unwrap()).unwrap()
    }
}
