#![feature(decl_macro)]
#![feature(type_alias_impl_trait)]

pub mod interface;
pub mod layout;
pub mod storage;

use chrono::Utc;
use interface::*;
use rand::prelude::SliceRandom;
use rocket::{catchers, launch, routes};
use storage::{Post, DATABASE};

#[launch]
fn rocket() -> _ {
    DATABASE
        .posts
        .insert(
            &0,
            &Post {
                parent: 0,
                id: 0,
                posts: 0,
                depth: 0,
                timestamp: Utc::now(),
                text: include_str!("../root-post").to_string(),
                author: Some("oxelpot".to_string()),
            },
        )
        .unwrap();

    rocket::build()
        .register("/", catchers![default_catcher])
        .mount(
            "/",
            routes![
                index,

                privacy,
                terms_of_use,
                licence,
                
                view_post,
                view_post_json,
                view_post_plain,
                view_post_plain_css,
                
                create_post,
                set_post_as_theme,
            ],
        )
}

pub fn subtitle() -> &'static str {
    [
        "combining the worst aspects of 4chan and reddit",
        "your place to post low-quality content",
        "powered by 15 .unwrap()'s and >226 dependencies",
        "WITHOUT ANY WARRANTY",
        "free as in 1 + 2",
        "the trash bin of the internet",
        "i like to interject for a moment",
        "almost better than facebook",
        "running in the cloud™",
        "comes DoS protection free",
    ]
    .choose(&mut rand::thread_rng())
    .unwrap()
}
