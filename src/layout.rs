use crate::interface::*;
use crate::subtitle;
use markup::Render;
use rocket::response;
use rocket::response::content::Html;
use rocket::response::Responder;
use rocket::uri;
use rocket::Request;

markup::define! {
    Layout<'a, Inner: markup::Render>(inner: Inner, theme_url: Option<&'a str>){
        @markup::doctype()
        html {
            head {
                title { "oxelpot" }
                @if let Some(theme_url) = theme_url {
                    link[rel="stylesheet", href=theme_url.to_string()];
                }
            }
            body {
                header {
                    a[href=uri!(index()).to_string()] { h1 { "oxelpot" } }
                    p { @subtitle() }
                    p { b{ "not ready for use yet! DO NOT USE!" } }
                }
                hr;
                div.inner { @inner }
                hr;
                footer {
                    "running oxelpot " @env!("CARGO_PKG_VERSION") " | "
                    a[href=uri!(licence()).to_string()] { "oxelpot is free software" } " | "
                    a[href=uri!(terms_of_use()).to_string()]{"terms of use"} " | "
                    a[href=uri!(privacy()).to_string()]{"privacy policy"}
                }
            }
        }
    }
}

pub struct Layouted<T>(pub T);
impl<'r, 'o: 'r, R: Render> Responder<'r, 'o> for Layouted<R> {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        Html(format!(
            "{}",
            Layout {
                inner: self.0,
                theme_url: req.cookies().get("theme_url").map(|c| c.value())
            }
        ))
        .respond_to(req)
    }
}
