use crate::{layout::Layouted, storage::Post};
use markup::Render;
use rocket::{
    catch,
    form::Form,
    get,
    http::{Cookie, CookieJar, Status},
    post,
    response::{
        content::{Css, Json, Plain},
        Redirect,
    },
    uri, FromForm, Request,
};
use std::fmt::Display;
use time::Duration;

markup::define! {
    PostMarkup<'a>(post: &'a Post, preview_depth: usize, truncate: bool) {
        a[href=view_post_url(post.id)] {"==>"} " "
        sub.meta {
            b.subpost_count { "(" @post.posts ")" }
            " by " @if let Some(author) = &post.author {
                b.author { @author }
            } else {
                b.author.anonymous { "anonymous user" }
            }
            " at " b.timestamp { @post.timestamp.to_rfc2822() }
        }
        @if *truncate {
            @let trunc = post.text.lines().take(3).map(|l| l.chars().take(128).collect::<String>()).collect::<Vec<_>>().join("\n");
            pre { @trunc }
            @if post.text.len() > trunc.len() { sup.truncated_note { i {"(content was truncated)"} } }
        } else {
            pre { @post.text }
        }
        @if *preview_depth > 0 {
            @if post.posts > 3 {
                a[href=view_post_url(post.id)] {"See replies"}
            } else if post.posts > 0 {
                ul.subpost_preview {
                    @for post in post.subposts() {
                        li.post { @PostMarkup{ post: &post, preview_depth: preview_depth - 1, truncate: *truncate } }
                    }
                }
            }
        }
    }

    PostActions<'a>(post: &'a Post) { div.post_actions {
        @let post_id = post.idstring();
        @if post.id != 0 { a.view_parent[href=view_post_url(post.parent)] { "view parent post" } }
        @if post.text.starts_with("/*") {
            form.set_post_as_theme[method="POST", action=uri!(set_post_as_theme(post_id.as_str())).to_string()] {
                input[type="submit", value="apply this posts content as stylesheet"];
            }
        }
        details.create_post {
            summary {"create post"}
            form.create_post_form[method="POST", action=uri!(create_post(post_id.as_str())).to_string(), id="answer"] {
                textarea[type="text", form="answer", name="text", rows="5", cols="40", placeholder="i love oxelpot"] {} br;
                "author (optional): " input[type="text", name="author", placeholder="anonymous"]; br;
                input[type="submit", value="post"];
            }
        }
    }}
}

#[get("/")]
pub fn index() -> Redirect {
    Redirect::to(view_post_url(0))
}

#[get("/terms-of-use")]
pub fn terms_of_use() -> Layouted<impl Render + Display> {
    Layouted(markup::new! {
        h2 {"Terms of Use"}
        pre {@include_str!("../terms-of-use")
    }})
}

#[get("/privacy")]
pub fn privacy() -> Layouted<impl Render + Display> {
    Layouted(markup::new! {
        h2 {"Privacy Policy"}
        pre {@include_str!("../privacy")
    }})
}

#[get("/licence")]
pub fn licence() -> Layouted<impl Render + Display> {
    Layouted(markup::new! {
        h2 {"Licence of the server software"}
        pre {@include_str!("../LICENCE")
    }})
}

#[catch(default)]
pub fn default_catcher(status: Status, _request: &Request) -> Layouted<impl Render + Display> {
    Layouted(markup::new! {
        h2 {"Error: " @status.code }
        p { @status.reason() }
    })
}

#[get("/<post>?<page>&<limit>", rank = 4)]
pub fn view_post(
    post: Post,
    page: Option<usize>,
    limit: Option<usize>,
) -> Result<Layouted<impl Render + Display>, std::string::String> {
    let preview_depth = match post.depth {
        0 => 0,
        1 => 0,
        2 => 1,
        _ => 3,
    };
    let limit = limit.unwrap_or(32).min(255).max(0);
    let page = page.unwrap_or(0).max(0);
    let skip = limit * page;
    Ok(Layouted(markup::new! {
        @PostActions{post:&post}
        @PostMarkup{post:&post,preview_depth:0,truncate:false}
        ul.subpost_listing {
            @for post in post.subposts().skip(skip).take(limit) {
                li.post { @PostMarkup{ post: &post, preview_depth, truncate: true } }
            }
        }
    }))
}

#[get("/<post>?format=json", rank = 1)]
pub fn view_post_json(post: Post) -> Json<String> {
    Json(serde_json::to_string(&post).unwrap())
}

#[get("/<post>?format=plain", rank = 2)]
pub fn view_post_plain(post: Post) -> Plain<String> {
    Plain(post.text)
}

#[get("/<post>?format=plain_css", rank = 3)]
pub fn view_post_plain_css(post: Post) -> Result<Css<String>, String> {
    if post.text.starts_with("/*") {
        Ok(Css(post.text))
    } else {
        Err("post does not qualify as a theme".to_string())
    }
}

pub fn view_post_url(id: u128) -> String {
    let id = id.to_string();
    uri!(view_post(
        id.as_str(),
        None as Option<usize>,
        None as Option<usize>
    ))
    .to_string()
}

#[derive(FromForm)]
pub struct NewPostForm<'a> {
    #[field(validate = neq(""))]
    text: &'a str,
    #[field(validate = len(..32))]
    author: &'a str,
}

#[post("/<parent>/post", data = "<form>")]
pub fn create_post(parent: Post, form: Form<NewPostForm>) -> Redirect {
    let mut post = parent.subpost_template();
    post.text = form.text.to_string();
    post.author = if form.author.len() != 0 {
        Some(form.author.to_string())
    } else {
        None
    };
    post.insert();
    Redirect::to(view_post_url(post.id))
}

#[post("/<post>/set_theme")]
pub fn set_post_as_theme(post: Post, jar: &CookieJar<'_>) -> Redirect {
    let post_id = post.idstring();
    let cookie = Cookie::build(
        "theme_url",
        uri!(view_post_plain_css(post_id.as_str())).to_string(),
    )
    .max_age(Duration::days(360));
    jar.add(cookie.finish());

    println!(
        "{}",
        Cookie::build(
            "theme_url",
            uri!(view_post_plain_css(post_id.as_str())).to_string(),
        )
        .max_age(Duration::days(360))
        .finish()
    );

    Redirect::to(view_post_url(post.id))
}
